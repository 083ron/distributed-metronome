from tkinter import *
import time

fp1 = open("words.txt",  "r").readlines()
fp2 = open("colours.txt", "r").readlines()

c1 = None
c2 = None

run_id = 0
running = False

ii = 0

frametime = int(521.74 * 4)
fade_dv = 30

root = Tk()
cclast = None
frm = Frame(root, bg="white")

def shownext(e=None,r_id=None):
    global c1, c2, tf, cclast, ii, run_id

    print("Shownext:", r_id)
    if not running or run_id != r_id:
        print("Shownext:", r_id, "Stopping.")
        return

    lbl.configure(text=fp1[ii].strip()) #.readline().strip())
    cc = fp2[ii] #.readline()

    if "-" in cc:
        cc = cclast

    bits = cc.split()

    c1 = [int(a) for a in bits[:3]]
    if len(bits) > 3:
        c2 = [int(a) for a in bits[3:6]]
        tf = 0.0
        root.after(10, fade)
    else:
        print("CL:",cl)
        color_label(c1)
    if "-" not in cc:
        cclast = cc
    ii += 1
    if ii >= len(fp1):
        ii = 0
    root.after(frametime, lambda r=run_id: shownext(r_id=r))

def color_label(c):
    lbl.configure(fg = "#%02x%02x%02x"%tuple(c))

def fade():
    global tf
    #print(tf, "entry: ")
    c3 = [0,0,0]
    #print(tf)
    if (tf>1.0):
        return
    for i in range(3):
        c3[i] = int( tf*c2[i] + (1.0-tf)*c1[i])
    color_label(c3)
    tf += 1.0 / fade_dv
    if tf<=1.0:
        #print(tf, "scheduling..")
        root.after(int(frametime/fade_dv), fade)

def begin(run_id):
    global root
    tnow = int(time.time()*1000.0)
    dly = (frametime*4) - (tnow%(frametime*4))
    root.after(dly, lambda r=run_id: shownext(r_id=r))
        
def clicked(e=None):
    global running, run_id, root, ii, button, frametime, bpm_entry
    run_id += 1
    running = not running
    if running:
        ii = 0
        bpm = int(bpm_entry.get())
        frametime = int(60000.0 / bpm)
        button.configure(text="STOP")
        root.after(1, lambda r=run_id: begin(r))
    else:
        button.configure(text="Start>>")
    
    
lbl = Label(frm, text=".", bg="white", font="Helvetica 126")
frm2 = Frame(root, bg="white")
bpm_label = Label(frm2, text = "BPM: ", bg="white", font="Helvetica 26")
bpm_entry = Entry(frm2, font="Helvetica 26")
button = Button(frm2, text="Start>>", font="Helvetica 26", command=clicked)

lbl.pack(expand=YES, fill=BOTH)
frm.pack(expand=YES, fill=BOTH)
bpm_label.pack(side=LEFT, fill=X)
bpm_entry.pack(side=LEFT)
bpm_entry.insert(END, "115")
button.pack(side=LEFT)
frm2.pack(fill=X)

lbl.bind("<1>", shownext)
root.geometry('600x500')
root.mainloop()
